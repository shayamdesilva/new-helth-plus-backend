package com.helthplus.springbootstarter.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.helthplus.springbootstarter.domain.UserValidationDTO;
import com.helthplus.springbootstarter.services.EmailService;

@RestController
@RequestMapping("api/email")
public class EmailController {
	
	@Autowired
	EmailService emailService;
	
	@GetMapping("/conformclinic/{email}")
	public UserValidationDTO verifyEmail(@PathVariable(value = "email",required = true) String email)
	{
		String value = email;
		return emailService.verifyEmailConformation(email);
	}
	
	@GetMapping("/verifydoctoremail/{email}")
	public UserValidationDTO verifyDoctorEmail(@PathVariable(value = "email",required = true) String email)
	{
		String value = email;
		return emailService.verifyDoctorEmailConformation(email);
	}

}
