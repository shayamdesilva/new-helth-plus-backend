package com.helthplus.springbootstarter.services;

import com.helthplus.springbootstarter.domain.RollDTO;
import com.helthplus.springbootstarter.domain.UserDTO;
import com.helthplus.springbootstarter.domain.UserValidationDTO;

public interface UserService {
	
	UserValidationDTO createUser(UserDTO userDTO);
	
	UserValidationDTO loginUser(UserDTO userDTO);
	
	UserValidationDTO getClinicUser(UserDTO userDTO);
	
	UserValidationDTO getSystemDoctorsByClinicId(UserDTO userDTO);
	
	
	UserValidationDTO createSystemUser(UserDTO userDTO);

}
