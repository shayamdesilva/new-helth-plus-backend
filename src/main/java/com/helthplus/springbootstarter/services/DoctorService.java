package com.helthplus.springbootstarter.services;

import com.helthplus.springbootstarter.domain.PatientDTO;
import com.helthplus.springbootstarter.domain.DoctorDTO;
import com.helthplus.springbootstarter.domain.UserValidationDTO;

public interface DoctorService {
	
	UserValidationDTO loginUser(String email,String password) ;
	
	UserValidationDTO getUserById(DoctorDTO dto) ;
	
	UserValidationDTO createUser(DoctorDTO dto) ;
	
	UserValidationDTO getAllDoctor();

}
